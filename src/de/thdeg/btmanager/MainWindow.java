package de.thdeg.btmanager;

import java.io.IOException;

import de.thdeg.btmanager.bluetooth.Bluetooth;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainWindow extends Application{
	
	private static int WIDTH = 800;
	private static int HEIGHT = 600;
	
	private Bluetooth bt;
	private static String deviceAddress;
	
	private TextArea log, response;
	private TextField[] rgbField;
	private ListView<String> devicesList;
	private ObservableList<String> devices = FXCollections.observableArrayList("HC-06, 98D33220A2AE", "MiBoard2.5, 001F818888BC", "TEST, 98D332113BB0");
	
	public static String testInformation = "";

	public void start(Stage stage) {
		Scene scene = new Scene(createRootElement(), WIDTH, HEIGHT);
		stage.setTitle("BT-Manager");
		
		bt = new Bluetooth();
		stage.setScene(scene);
		stage.show();
	}
	

	private Parent createRootElement() {
		BorderPane root = new BorderPane();
		
		root.setTop(createMenuElements());
		root.setLeft(createDiscoveryElements());
		root.setBottom(createLogElements());
		root.setCenter(createRGBView());
		
		return root;
	}
	
	private Parent createRGBView() {
		VBox root = new VBox();
		
		HBox led1 = new HBox();
		HBox led2 = new HBox();
		HBox led3 = new HBox();
		HBox led4 = new HBox();
		HBox led5 = new HBox();
		
		root.setSpacing(10);
		
		rgbField = new TextField[15];
		for(int i = 0; i < rgbField.length; i++) {
			rgbField[i] = new TextField();
			rgbField[i].setMaxWidth(50);
		}
		
		led1.getChildren().addAll(rgbField[0], rgbField[1], rgbField[2]);
		led2.getChildren().addAll(rgbField[3], rgbField[4], rgbField[5]);
		led3.getChildren().addAll(rgbField[6], rgbField[7], rgbField[8]);
		led4.getChildren().addAll(rgbField[9], rgbField[10], rgbField[11]);
		led5.getChildren().addAll(rgbField[12], rgbField[13], rgbField[14]);
		
		root.getChildren().addAll(led1, led2, led3, led4, led5);
		
		return root;
	}
	
	private Parent createLogElements() {
		VBox root = new VBox();
		HBox buttons = new HBox();
		HBox texts = new HBox();
		
		Button clearBtn = new Button("Clear Log");
		
		log = new TextArea();
		response = new TextArea();
		log.setEditable(false);
		response.setEditable(false);
		
		clearBtn.setOnAction(e->log.clear());
		
		buttons.getChildren().add(clearBtn);
		texts.getChildren().addAll(log, response);
		root.getChildren().addAll(texts, buttons);
		
		return root;
	}
	
	private Parent createDiscoveryElements() {
		VBox root = new VBox();
		
		HBox buttons = new HBox();
		VBox fields = new VBox();
		
		fields.setPadding(new Insets(10));
		fields.setSpacing(10);
		
		buttons.setPadding(new Insets(10));
		buttons.setSpacing(10);
		
		// BUTTONS
		Button discovery = new Button("Discovery");
		Button connect = new Button("Connect");
		Button disconnect = new Button("Disconnect");
		Button send = new Button("Send");
		
		// TEXT
		TextField input = new TextField();
		input.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				int length = newValue.length() - 1;
				if (length < 0) return;
				String s = newValue.substring(newValue.length() - 1);
				System.out.println(s);
				sendChar(s);
				input.clear();
			}
		});
		
		connect.setDisable(true);
		connect.setOnAction(e-> connectToDevice(deviceAddress));
		
		//disconnect.setDisable(true);
		disconnect.setOnAction(e -> disconnect());
		
		send.setOnAction(e -> sendChar());
		
		discovery.setOnAction(e-> startDiscovery());
		
		
		// VIEW
		devicesList = new ListView<String>();
		devicesList.setItems(devices);
		devicesList.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				connect.setDisable(false);
				deviceAddress = devicesList.getSelectionModel().getSelectedItem().toString();
				int index = deviceAddress.indexOf(',');
				deviceAddress = deviceAddress.substring(index + 2);
				System.out.println(deviceAddress);
				
			}
		});
		
		buttons.getChildren().addAll(discovery, connect, disconnect, send);
		fields.getChildren().addAll(input, devicesList);
		root.getChildren().addAll(buttons, fields);
		return root;
	}
	
	private Parent createMenuElements() {
		MenuBar menuBar = new MenuBar();
		
		Menu connection = new Menu("Connection...");
		MenuItem settings = new MenuItem("Settings");
		
		connection.getItems().add(settings);
		
		menuBar.getMenus().add(connection);
		
		return menuBar;
	}
	
	private void sendChar() {
		System.out.println(testInformation);
		byte[] rgb = new byte[16];
		rgb[0] = (byte) 0x47;
		for(int i = 1; i < rgb.length; i++) {
			String value = rgbField[i - 1].getText().toString();
			if(value.isEmpty()) rgb[i] = 0x00;
			else rgb[i] = (byte) Integer.parseInt(value);
		}
		try {
			bt.write(rgb);
			log.appendText("Send\n");
			delay(200);
			response.appendText(bt.read() + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void sendChar(String s) {
		try {
			bt.write(s.getBytes());
			log.appendText("Send " + s + "\n" );
			delay(200);
			response.appendText(bt.read() + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void disconnect() {
		bt.disconnect();
	}
	
	private void connectToDevice(String address) {
		try {
			bt.connectToDevice(address);
			log.appendText("Connected to " + address + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private void startDiscovery() {
		bt.startDiscovery();
		// TODO: fill list
		System.out.println(Bluetooth.deviceURL);
	}
	
	private void delay(int ms) {
		try {
			Thread.sleep(ms);
		}catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public TextArea getLogger() {return log;}
	public ListView<String> getDevicesList(){return devicesList;}
}
