package de.thdeg.btmanager.bluetooth;

import java.io.IOException;

import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;

import de.thdeg.btmanager.MainWindow;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextArea;

public class BTDiscoveryListener implements DiscoveryListener{

	@Override
	public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
		try {
			Bluetooth.devices.clear();
			String name = btDevice.getFriendlyName(false);
//			if(name.matches("MiBoard.*")) {
//				Bluetooth.device = btDevice;
//			}
			Bluetooth.device = btDevice;
			Bluetooth.devices.add(btDevice.getFriendlyName(false) + ", " + btDevice.getBluetoothAddress());	
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void inquiryCompleted(int discType) {
		synchronized(Bluetooth.lock) {
			Bluetooth.lock.notify();
		}
	}

	@Override
	public void serviceSearchCompleted(int transID, int respCode) {
		synchronized(Bluetooth.lock) {
			Bluetooth.lock.notify();
		}
	}

	@Override
	public void servicesDiscovered(int arg0, ServiceRecord[] servRecord) {
		for(int i = 0; i < servRecord.length; i++) {
			Bluetooth.deviceURL = servRecord[i].getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
			
			if(Bluetooth.deviceURL != null) {
				break;
			}
		}
	}
}
