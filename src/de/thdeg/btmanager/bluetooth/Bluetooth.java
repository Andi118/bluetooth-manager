package de.thdeg.btmanager.bluetooth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

public class Bluetooth{
	
	protected static RemoteDevice device;
	public static String deviceURL;
	protected static List<String> devices = new ArrayList<String>();
	
	private UUID uuid = new UUID(0x1101);
	private UUID[] searchUUIDSet = new UUID[] {uuid};
	private int[] attrIDs = new int[] {0x0100};
	
	protected static Object lock = new Object();
	
	private BTDiscoveryListener discoveryListener;
	private StreamConnection streamConnection;
	private OutputStream os;
	private InputStream is;
	
	private boolean discovering = false;
	
	public Bluetooth() {
		discoveryListener = new BTDiscoveryListener();
	}
	
	public void startDiscovery() {
		if(discovering) return;
		try {
			scanForDevices();
			searchForServices();
			discovering = false;
			
		} catch (IOException e) {
			e.printStackTrace();
			discovering = false;
		}
	}

	private void scanForDevices() throws IOException {
		LocalDevice.getLocalDevice().getDiscoveryAgent().startInquiry(DiscoveryAgent.GIAC, discoveryListener);
		try {
			synchronized(lock) {
				lock.wait();
			} 
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void searchForServices() throws IOException {
		LocalDevice.getLocalDevice().getDiscoveryAgent().searchServices(attrIDs, searchUUIDSet, device, discoveryListener);
		try {
			synchronized(lock) {
				lock.wait();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Hallo:" + deviceURL);
	}
	
	public void connectToDevice(String address) throws IOException {
		streamConnection = (StreamConnection) Connector.open("btspp://"+ address + ":1;authenticate=false;encrypt=false;master=false"); // Problem
		os = streamConnection.openOutputStream();
		is = streamConnection.openInputStream();
	}
	
	public String read() {
		String message = "";
		try {
			while(is.available() != 0) {
				message = message + (char)is.read();
			}
			
			return message;
		} catch (IOException e) {
			e.printStackTrace();
			return "nope";
		}
	}
	
	public void write(byte[] command) throws IOException {
		os.write(command);
		os.flush();
	}
	
	public void disconnect() {
		if(streamConnection == null) return;
		try {
			os.close();
			is.close();
			streamConnection.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public List<String> getDivices(){ return devices; }
}
